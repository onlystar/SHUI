## Introduction
> A Vue.js 2.0 UI Toolkit for Web

## Install
`npm install xh-ui --save`

## Quick Start
`import Vue from 'vue'`<br/>
`import XHUI from 'xh-ui'`<br/>
`import 'xh-ui/theme-default/lib/index.css'`<br/>
`Vue.use(XHUI)`

## Author
>Rain
