## 介绍

`xh-ui` 是一款基于 `Vue.js 2.0` 的前端 `UI`组件库, 其中文件夹`package`为`xh-ui`源码
整个项目为`xh-ui`的API

## 在线演示

[演示文档](http://xhui.onlystar.site)

## 特性

- 基于 `Vue` 开发的 `UI` 组件
- 使用 `npm + webpack + babel` 的工作流，支持 ES6
- 提供友好的 API，可灵活的使用组件

## 启动
>1. npm intall
>2. npm run dev

## 使用xh-ui
>1. `npm install --save xh-ui`
>2. 在Vue项目里的`main.js`
>>`import XHUI from 'xh-ui'`<br/>
>>`import 'xh-ui/theme-default/lib/index.css'`<br/>
>>`Vue.use(XHUI)`

